import Util._

object Main extends Parser {

  def isnumericalval(t: term): Boolean = t match {
    case TmZero => true
    case TmSucc(t1) if isnumericalval(t1) => true
    case _ => false
  }

  def isval(t: term): Boolean = t match {
    case TmTrue => true
    case TmFalse => true
    case t if isnumericalval(t) => true
    case _ => false
  }

  case class NoRuleApplies(s: String) extends Exception(s)

  def eval(t: term): term = t match {
    case t if isval(t) => t                                                     //B-Value
    case TmIf(t1, t2, t3) if eval(t1) == TmTrue => eval(t2)                     //B-IfTrue
    case TmIf(t1, t2, t3) if eval(t1) == TmFalse => eval(t3)                    //B-IfFalse
    case TmSucc(t1) if isnumericalval(eval(t1)) => TmSucc(eval(t1))             //B-Succ
    case TmPred(t1) =>
      val v1 = eval(t1)
      v1 match {
        case TmZero => TmZero                                                   //B-PredZero
        case TmSucc(nv1) if isnumericalval(nv1) => nv1                          //B-PredSucc
        case _ => throw new NoRuleApplies("no rule")
      }
    case TmIsZero(t1) =>
      val v1 = eval(t1)
      v1 match {
        case TmZero => TmTrue                                                   //B-IsZeroZero
        case TmSucc(nv1) if isnumericalval(nv1) => TmFalse                      //B-IsZeroSucc
        case _ => throw new NoRuleApplies("no rule")
      }
    case _ => throw new NoRuleApplies("no rule")
  }

  def ownTests(input: String) = {
    println(input+" => "+eval(term.apply(input)))
    // var input = ""
    // while(input != "exit"){
    //   input = scala.io.StdIn.readLine("expression> ")
    //   println(input)
    //   println(eval(term.apply(input)))
    // }
  }
}