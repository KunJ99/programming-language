import scala.util.parsing.combinator._
import Util._

trait Parser {

  trait term
  case object TmTrue extends term
  case object TmFalse extends term
  case class TmIf(condE: term, thenE: term, elseE: term) extends term
  case object TmZero extends term
  case class TmSucc(num: term) extends term
  case class TmPred(num: term) extends term
  case class TmIsZero(num: term) extends term

  object term extends RegexParsers {
    def wrap[T](rule: Parser[T]): Parser[T] = rule | "(" ~> rule <~ ")"
    lazy val int: Parser[Int] = """-?\d+""".r ^^ (_.toInt)
    lazy val str: Parser[String] = """[a-zA-Z0-9]+""".r
    lazy val expr: Parser[term] =
      int                                                 ^^ { case 0 => TmZero }                         |
      "true"                                              ^^ { case _ => TmTrue }                         |
      "false"                                             ^^ { case _ => TmFalse }                        |
      wrap("if" ~> expr ~ "then" ~ expr ~ "else" ~ expr)  ^^ { case c ~ _ ~ t ~ _ ~ e => TmIf(c, t, e) }  |
      wrap("succ" ~> expr)                                ^^ { case n => TmSucc(n) }                      |
      wrap("pred" ~> expr)                                ^^ { case n => TmPred(n) }                      |
      wrap("iszero" ~> expr)                              ^^ { case n => TmIsZero(n) }                
    def apply(str: String): term = parse(expr, str).getOrElse(error(s"bad syntax: $str"))
  }

}