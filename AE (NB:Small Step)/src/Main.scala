import Util._

object Main extends Parser {

  def isnumericalval(t: term): Boolean = t match {
    case TmZero => true
    case TmSucc(t1) if isnumericalval(t1) => true
    case _ => false
  }

  def isval(t: term): Boolean = t match {
    case TmTrue => true
    case TmFalse => true
    case t if isnumericalval(t) => true
    case _ => false
  }

  case class NoRuleApplies(s: String) extends Exception(s)

  def eval1(t: term): term = t match {
    case TmIf(TmTrue, t2, t3) => t2                               //E-IfTrue
    case TmIf(TmFalse, t2, t3) => t3                              //E-IfFalse
    case TmIf(t1, t2, t3) =>                                      //E-If
      val s1 = eval1(t1)
      TmIf(s1, t2, t3)
    case TmSucc(t1) =>                                            //E-Succ
      val s1 = eval1(t1)
      TmSucc(s1)
    case TmPred(TmZero) => TmZero                                 //E-PredZero
    case TmPred(TmSucc(nv1)) if isnumericalval(nv1) => nv1        //E-PredSucc
    case TmPred(t1) =>                                            //E-Pred
      val s1 = eval1(t1)
      TmPred(s1)
    case TmIsZero(TmZero) => TmTrue                               //E-IsZeroZero
    case TmIsZero(TmSucc(nv1)) if isnumericalval(nv1) => TmFalse  //E-IsZeroSucc
    case TmIsZero(t1) =>                                          //E-IsZero
      val s1 = eval1(t1)
      TmIsZero(s1)
    case _ => throw new NoRuleApplies("no rule")
  }

  def eval(t: term): term = {
    try{
      val s = eval1(t)
      eval(s)
    } catch {
        case NoRuleApplies(_) => t
    }
  }

  def ownTests(input: String) = {
    println(input+" => "+eval(term.apply(input)))
    // var input = ""
    // while(input != "exit"){
    //   input = scala.io.StdIn.readLine("expression> ")
    //   println(input)
    //   println(eval(term.apply(input)))
    // }
  }
}