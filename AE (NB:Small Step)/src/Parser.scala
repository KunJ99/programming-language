import scala.util.parsing.combinator._
import Util._

trait Parser {

  trait Term
  case object TmTrue extends Term
  case object TmFalse extends Term
  case class TmIf(condE: Term, thenE: Term, elseE: Term) extends Term
  case object TmZero extends Term
  case class TmSucc(num: Term) extends Term
  case class TmPred(num: Term) extends Term
  case class TmIsZero(num: Term) extends Term

  object Term extends RegexParsers {
    def wrap[T](rule: Parser[T]): Parser[T] = rule | "(" ~> rule <~ ")"
    lazy val int: Parser[Int] = """-?\d+""".r ^^ (_.toInt)
    lazy val str: Parser[String] = """[a-zA-Z0-9]+""".r
    lazy val expr: Parser[Term] =
      int                                                 ^^ { case 0 => TmZero }                         |
      "true"                                              ^^ { case _ => TmTrue }                         |
      "false"                                             ^^ { case _ => TmFalse }                        |
      wrap("if" ~> expr ~ "then" ~ expr ~ "else" ~ expr)  ^^ { case c ~ _ ~ t ~ _ ~ e => TmIf(c, t, e) }  |
      wrap("succ" ~> expr)                                ^^ { case n => TmSucc(n) }                      |
      wrap("pred" ~> expr)                                ^^ { case n => TmPred(n) }                      |
      wrap("iszero" ~> expr)                              ^^ { case n => TmIsZero(n) }                
    def apply(str: String): Term = parse(expr, str).getOrElse(error(s"bad syntax: $str"))
  }

}