import Util._

object Main extends Parser {

  /* Shifting */

  def tmmap(onvar: (Int, Int, Int) => Term, c: Int, t: Term): Term = walk(onvar, c, t)
    
  def walk(onvar: (Int, Int, Int) => Term, c: Int, t: Term): Term = t match {
      case TmVar(x, n) => onvar(c, x, n)
      case TmAbs(x, t2) => TmAbs(x, walk(onvar, c+1, t2))
      case TmApp(t1, t2) => TmApp(walk(onvar, c, t1), walk(onvar, c, t2))
    }

  def termShiftAbove(d: Int, c: Int, t: Term): Term =
    tmmap(
      ((c, x, n) =>
      if(x >= c) TmVar(x+d, n+d)
      else TmVar(x, n+d))
    , c, t)
  
  def termShift(d: Int, t: Term): Term = termShiftAbove(d, 0, t)

  /* Substitution */

  def termSubst(j: Int, s: Term, t: Term): Term =
    tmmap(
      ((c, x, n) =>
      if(x == j+c) termShift(c, s)
      else TmVar(x, n))
    , 0, t)

  def termSubstTop(s: Term, t: Term): Term =
    termShift(-1, termSubst(0, termShift(1, s), t))

  /* Evaluation */

  def isval(ctx: Context, t: Term): Boolean = t match {
    case TmAbs(_, _) => true
    case _ => false
  }

  case class NoRuleApplies(s: String) extends Exception(s)

  def eval1(ctx: Context, t: Term): Term = t match {
    case TmApp(TmAbs(x, t12), v2) if isval(ctx, v2) => termSubstTop(v2, t12)
    case TmApp(v1, t2) if isval(ctx, v1) =>
      val s2 = eval1(ctx, t2)
      TmApp(v1, s2)
    case TmApp(t1, t2) => 
      val s1 = eval1(ctx, t1)
      TmApp(s1, t2)
    case _ => throw new NoRuleApplies("no rule")
  }

  def eval(ctx: Context, t: Term): Term = {
    try{
      val s = eval1(ctx, t)
      eval(ctx, s)
    } catch {
        case NoRuleApplies(_) => t
    }
  }

  /* Printing */

  def printtm(ctx: Context, t: Term): Unit = t match {
    case TmAbs(x, t1) =>
      val (ctx1, x1) = pickfreshname(ctx, x)
      print("(λ" + x1 + ". ")
      printtm(ctx1, t1)
      print(")")
    case TmApp(t1, t2) => 
      print("(")
      printtm(ctx, t1)
      print(" ")
      printtm(ctx, t2)
      print(")")
    case TmVar(x, n) =>
      if(ctxlength(ctx) == n) print(index2name(ctx, x))
      else print("[bad index]")
  }

  /* Testing */

  def ownTests(input: String) = {
    print(input + " => ")
    print(eval(List(), Term.apply(input)(List())))
    println()
  }
}