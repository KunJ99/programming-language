import scala.util.parsing.combinator._
import Util._

trait Parser {

  /* Datatypes */

  trait Term
  case class TmVar(index: Int, total: Int) extends Term
  case class TmAbs(param: String, bodyE: Term) extends Term
  case class TmApp(funE: Term, valE: Term) extends Term

  trait Binding
  case object NameBind extends Binding

  type Context = List[(String, Binding)]

  /* Context Management */

  def ctxlength(ctx: Context): Int = ctx.length

  def addbinding(ctx: Context, x: String, bind: Binding): Context = (x, bind) :: ctx

  def addname(ctx: Context, x: String): Context = addbinding(ctx, x, NameBind)

  def isnamebound(ctx: Context, x: String): Boolean = ctx match {
    case (y, _) :: rest => 
      if(y == x) true
      else isnamebound(rest, x)
    case Nil => false
  }

  def pickfreshname(ctx: Context, x: String): (Context, String) = 
    if(isnamebound(ctx, x)) pickfreshname(ctx, x+"'")
    else ((x, NameBind) :: ctx, x)

  def index2name(ctx: Context, x: Int): String =
    try{
      val (xn, _) = ctx.apply(x)
      xn
    } catch {
      case _: IndexOutOfBoundsException => error(s"Variable lookup failure: offset: $x, ctx size: $ctx.length")
    }

  def name2index(ctx: Context, x: String): Int = ctx match {
    case (y, _) :: rest => 
      if(y == x) 0
      else 1 + name2index(rest, x)
    case Nil => error(s"Identifier $x is unbound")
  }

  /* Parsing */

  object Term extends RegexParsers {
    lazy val str: Parser[String] = """[a-zA-Z0-9]+""".r
    lazy val termE: Parser[Context => Term] =
      apptermE ^^ { case e => e}                      |
      "λ" ~> str ~ ". " ~ termE ^^ {
        case x ~ _ ~ e =>
          (ctx: Context) =>
            TmAbs(x, e(addname(ctx, x))) }                

    lazy val apptermE: Parser[Context => Term] =
      atermE ^^ { case e => e}                        |
      apptermE ~ atermE ^^ {
        case f ~ v =>
          (ctx: Context) =>
            TmApp(f(ctx), v(ctx)) }

    lazy val atermE: Parser[Context => Term] =
      "(" ~> termE <~ ")" ^^ { case e => e}                   |
      str ^^ {
        case x =>
          (ctx: Context) =>
            TmVar(name2index(ctx, x), ctxlength(ctx)) }

    def apply(str: String): (Context => Term) = parse(termE, str).getOrElse(error(s"bad syntax: $str"))
  }

}